*** Settings ***
Library    SeleniumLibrary
*** Variables ***
${btnSelectWordfile}    xpath=//input[@type="file"]
${imgFile}              ${CURDIR}/testfile.png
${titleFileOnQueue}     xpath=//span[@title="testfile.png"]
*** Test Cases ***
Test Upload File
    Open Browser      https://imagetopdf.com/    Chrome
    Upload file
    #Check File Display On Queue
*** Keywords ***
Upload file
    Wait Until Page Contains Element   ${btnSelectWordfile}
    Choose File     ${btnSelectWordfile}     ${imgFile}
