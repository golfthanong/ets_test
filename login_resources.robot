*** Settings ***
Library           SeleniumLibrary

*** Variables ***
${LOGIN URL}       http://10.157.0.141:8443/login
#${LOGIN URL}      https://account.line.biz/login?redirectUri=https://oaplus.line.biz
#${LOGIN URL}      http://www.google.com
${BROWSER}        chrome
${btnSelectWordfile}    xpath=//input[@type="file"]
${imgFile}              ${CURDIR}/testfile.png

*** Keywords ***
Open my Browser
    Open Browser    ${LOGIN URL}    browser=${BROWSER}
    Maximize Browser Window
    

    

Close Browsers
    Close All Browsers
    
Open Login Page
    Set Selenium Speed    10
    Go To    ${LOGIN URL}

Input username
    [Arguments]    ${username}
    Input Text     id=username_field    ${username}

Input pwd
    [Arguments]    ${password}
    Input Password    id=password_field    ${password}

Input description
    [Arguments]    ${description}
    Input Text     id=description_field    ${description}    

Input subject
    [Arguments]    ${subject}
    Input Text     id=subject_field    ${subject}    

click login button
    Click Button    id=login_button

welcome page should be visible
    Title Should Be    Welcome Page

Error page should be visible
    Title Should Be    Error Page