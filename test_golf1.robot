*** Settings ***
Library    Selenium2Library

***Test Cases ***
เปิด portal-trn ด้วย Chrome
    Open Browser   https://portal-trn.gfmis.go.th/login    chrome
    Maximize Browser Window
    Set Selenium Speed    1
เข้าหน้า login
    Click Element  XPath: /html/body/app-root/app-login-page/div/mat-card/div[1]/div[2]/div/div[1]/button/span
    Set Selenium Speed    1
กรอก User Password
    Input Text    //*[@id="username"]    T03004000201001
    Set Selenium Speed    1
    Input Password    //*[@id="password"]    Test@2022
    Set Selenium Speed    1
    Click Button    //*[@id="loginForm"]/div[5]/div/button
ปิด web Browser  
    Close Browser
