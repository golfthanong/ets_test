*** Settings ***
Library           DataDriver    DataDriven.xlsx
Resource          login_resources.robot

Suite Setup       Open my Browser
#Suite Teardown    Close Browsers
Test Setup        Open Login Page
Test Template     Invalid Login



*** Test Cases ***
Login with user '${username}' and password '${password}' and subject '${subject}' and description '${description}'     Default    UserData

*** Keywords ***
Invalid login
    Set Selenium Speed    1
    #Click Element  XPath: /html/body/div[2]/div/div[3]/div/div[3]/div[2]/a
    Set Selenium Speed    1
    [Arguments]    ${username}     ${password}     ${subject}     ${description}         
    [Tags]    FLAT
    
    #ใส่ login user
    Input Text    XPath: //*[@id="floatingInput"]    ${username}    
    Set Selenium Speed    1
    
    #ใส่ Password
    Input Password   XPath: //*[@id="floatingPassword"]    ${password}   
    Set Selenium Speed    1
    
    #กด login เพื่อเข้าweb
    Click Button    XPath: /html/body/div/app-root/app-login/div/div/div/div[2]/form/button   
    Wait Until Page Contains Element    XPath:/html/body/div[1]/app-root/app-overview/div/app-home/app-toolbar/mat-toolbar/div/div[2]/button
    
    #กดปุ่มสร้าง
    Click Button    XPath:/html/body/div[1]/app-root/app-overview/div/app-home/app-toolbar/mat-toolbar/div/div[2]/button
    
    #เลือก Module 
    Select From List By Index    //*[@id="left"]/select    1
    #เปิดปฏิทิน วันที่รับงาน
    Click Button    //*[@id="button-addon2"]
    #เลือก วันที่รับงาน
    Click Element   //*[@id="mat-datepicker-0"]/div/mat-month-view/table/tbody/tr[3]/td[1]/div[1]
    #เลือก ประเภทงาน
    Select From List By Index    XPath:/html/body/div[1]/app-root/app-overview/div/app-create/div/div/div/app-work-form/div/form/div/div[15]/select    1
    #ส่วนราชการผู้ขอ
    Select From List By Index   XPath:/html/body/div[1]/app-root/app-overview/div/app-create/div/div/div/app-work-form/div/form/div/div[17]/select   1
    #ผู้รับผิดชอบ
    Select From List By Index   XPath:/html/body/div[1]/app-root/app-overview/div/app-create/div/div/div/app-work-form/div/form/div/div[20]/select    1
    #ใส่เรื่อง
    Input Text     //*[@id="subject"]     ${subject}
    #รายละเอียด
    Input Text     //*[@id="description"]    ${description}
    #วันที่กำหนดเสร็จ
    Click Button     XPath:/html/body/div[1]/app-root/app-overview/div/app-create/div/div/div/app-work-form/div/form/div/div[26]/div/button
    #เลือก วันที่กำหนดเสร็จ
    Click Element    //*[@id="mat-datepicker-1"]/div/mat-month-view/table/tbody/tr[4]/td[1]/div[1]
    #กดบันทึกข้อมูล
    Click Button     XPath:/html/body/div[1]/app-root/app-overview/div/app-create/div/div/div/app-work-form/div/form/div/div[38]/button
    #กดยืนยัน
    Click Button    //*[@id="mat-dialog-0"]/app-confirm-dialog/div[2]/div[1]/button
    #กดตกลง
    Click Button   //*[@id="mat-dialog-1"]/app-confirm-dialog/div[3]/div/button
    #Wait Until Page Contains Element   ${btnSelectWordfile}
    #Choose File     ${btnSelectWordfile}     ${imgFile}

    #C  //*[@id="bp-sidebar"]/div/div[2]/ul/li[5]/div/div
    #Set Selenium Speed   2
    #Click Element  //*[@id="bp-sidebar"]/div/div[2]/ul/li[5]/div/ul/li[1]
    #Set Selenium Speed   3
    #Click Button  //*[@id="top"]/div[1]/button/svg
    # Error page should be visible
    #Click Link     XPath: //*[@id="bp-content"]/div[1]/div/div/div[2]/div/div[2]/div/div/div[2]/div[2]/span
   